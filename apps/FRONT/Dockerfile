FROM node:18-alpine AS base


FROM base AS builder

RUN apk add --no-cache libc6-compat
RUN apk update

WORKDIR /app
RUN yarn global add turbo
COPY . .
RUN turbo prune --scope=@ada/frontend --docker

FROM base AS installer
RUN apk add --no-cache libc6-compat
RUN apk update
WORKDIR /app

COPY .gitignore .gitignore
COPY --from=builder /app/out/json/ .
COPY --from=builder /app/out/yarn.lock ./yarn.lock
RUN yarn install

COPY --from=builder /app/out/full/ .
COPY turbo.json turbo.json

RUN yarn turbo build --filter=@ada/frontend

FROM base AS runner
WORKDIR /app

RUN addgroup --system --gid 1001 nodejs
RUN adduser --system --uid 1001 vitejs
USER vitejs

COPY --from=installer --chown=vitejs:nodejs /app/apps/FRONT/dist ./

USER root

RUN npm install -g vite

EXPOSE 3001

CMD ["vite", "preview", "--host", "--port", "3001"];

