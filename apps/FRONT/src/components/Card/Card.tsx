import { FiEdit } from 'react-icons/fi';
import { List, type Card as ICard } from '@ada/types';
import { Card as UICard, Button } from '@ada/ui';
import { useState } from 'react';
import {
  DeleteButton,
  MarkdownRenderer,
  BackButton,
  ForwardButton,
  CardForm,
} from '@/components';

export function Card({ title, content, id, list }: ICard) {
  const currentCard: ICard = { id, title, content, list };

  const [isEditMode, setIsEditMode] = useState(false);

  if (isEditMode) {
    return (
      <CardForm
        content={content}
        dataTestId='edit'
        id={id}
        list={list}
        setEditMode={setIsEditMode}
        title={title}
      />
    );
  }

  return (
    <UICard dataTestId={`card-visual-${id}`}>
      <UICard.Title>
        {title}
        <Button
          data-testid='edit-button'
          onClick={() => {
            setIsEditMode((prev) => !prev);
          }}
          style={{ backgroundColor: 'transparent' }}
          type='button'
        >
          <FiEdit color='#aaa' />
        </Button>
      </UICard.Title>
      <MarkdownRenderer markdownContent={content} />
      <UICard.Footer>
        {list !== List.TODO && <BackButton card={currentCard} />}
        <DeleteButton cardId={id} />
        {list !== List.DONE && <ForwardButton card={currentCard} />}
      </UICard.Footer>
    </UICard>
  );
}
