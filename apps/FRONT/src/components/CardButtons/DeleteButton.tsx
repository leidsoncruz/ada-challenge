import { Button } from '@ada/ui';
import { RiDeleteBin6Line } from 'react-icons/ri';
import { useDeleteCard } from '@/hooks';

interface DeleteButtonProps {
  cardId: string;
}

export function DeleteButton({ cardId }: DeleteButtonProps) {
  const { mutate: deleteCard } = useDeleteCard();

  const handleDeleteCard = () => {
    deleteCard(cardId);
  };

  return (
    <Button
      data-testid='delete-button'
      onClick={handleDeleteCard}
      style={{ backgroundColor: 'red' }}
    >
      <RiDeleteBin6Line />
    </Button>
  );
}
