import { parse } from 'marked';
import DomPurify from 'dompurify';

interface MarkdownRendererProps {
  markdownContent: string;
}

export function MarkdownRenderer({ markdownContent }: MarkdownRendererProps) {
  const htmlContent = DomPurify.sanitize(parse(markdownContent) as string);

  return <div dangerouslySetInnerHTML={{ __html: htmlContent }} />;
}
