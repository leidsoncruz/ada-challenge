import type { CardDTO, Card as ICard } from '@ada/types';
import { Button, Card, Input, TextArea } from '@ada/ui';
import DomPurify from 'dompurify';
import type { Dispatch, FormEvent, SetStateAction } from 'react';
import { useCreateCard, useUpdateCard } from '@/hooks';

interface FormElements extends HTMLFormControlsCollection {
  title: HTMLInputElement;
  content: HTMLTextAreaElement;
}
interface FormElement extends HTMLFormElement {
  readonly elements: FormElements;
}

interface CardFormProps extends Partial<ICard> {
  setEditMode?: Dispatch<SetStateAction<boolean>>;
  dataTestId?: string;
}

export function CardForm({
  id,
  title,
  content,
  list,
  setEditMode,
  dataTestId = 'create',
}: CardFormProps) {
  const { mutate: createCard } = useCreateCard();

  const { mutate: updateCard } = useUpdateCard();

  const handleSubmit = (e: FormEvent<FormElement>) => {
    e.preventDefault();

    const sanatizedTitle = DomPurify.sanitize(
      e.currentTarget.elements.title.value
    );
    const sanatizeContent = DomPurify.sanitize(
      e.currentTarget.elements.content.value
    );

    if (id && list) {
      const updateCardDTO: ICard = {
        title: sanatizedTitle,
        content: sanatizeContent,
        list,
        id,
      };

      updateCard(updateCardDTO, {
        onSuccess: () => {
          setEditMode!(false);
        },
      });
    } else {
      const createCardDTO: CardDTO = {
        title: sanatizedTitle,
        content: sanatizeContent,
        list,
      };

      createCard(createCardDTO);
    }

    e.currentTarget.reset();
  };

  const handleReset = (e: FormEvent<FormElement>) => {
    e.currentTarget.reset();
    if (id) {
      setEditMode!(false);
    }
  };

  return (
    <Card dataTestId={`card-form-${dataTestId}`}>
      <form onReset={handleReset} onSubmit={handleSubmit}>
        <Input
          defaultValue={title}
          name='title'
          placeholder='Título'
          required
          type='text'
        />
        <TextArea defaultValue={content} name='content' required />
        <Card.Footer>
          <Button style={{ color: '#040f13' }} type='reset' variant='outlined'>
            Discard
          </Button>
          <Button type='submit'>Save</Button>
        </Card.Footer>
      </form>
    </Card>
  );
}
