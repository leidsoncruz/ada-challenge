import { List, type Card } from '@ada/types';
import { MdOutlineArrowBack } from 'react-icons/md';
import { NavigateButton } from './NavigateButton';
import { useUpdateCard } from '@/hooks';

interface BackButtonProps {
  card: Card;
}

export function BackButton({ card }: BackButtonProps) {
  const { list, id, title, content } = card;

  const { mutate: updateCard } = useUpdateCard();

  const handleMoveCard = () => {
    const newColumn = list === List.DONE ? List.DOING : List.TODO;

    const updatedCard: Card = {
      list: newColumn,
      id,
      title,
      content,
    };

    updateCard(updatedCard);
  };

  return (
    <NavigateButton
      dataTestId='move-back'
      onClick={handleMoveCard}
      position='prev'
    >
      <MdOutlineArrowBack />
    </NavigateButton>
  );
}
