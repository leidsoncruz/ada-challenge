import { MdOutlineArrowForward } from 'react-icons/md';
import { List, type Card } from '@ada/types';
import { NavigateButton } from './NavigateButton';
import { useUpdateCard } from '@/hooks';

interface ForwardButtonProps {
  card: Card;
}

export function ForwardButton({ card }: ForwardButtonProps) {
  const { list, id, title, content } = card;

  const { mutate: updateCard } = useUpdateCard();

  const handleMoveCard = () => {
    const newColumn = list === List.TODO ? List.DOING : List.DONE;

    const updatedCard: Card = {
      list: newColumn,
      id,
      title,
      content,
    };

    updateCard(updatedCard);
  };

  return (
    <NavigateButton
      dataTestId='move-forward'
      onClick={handleMoveCard}
      position='next'
    >
      <MdOutlineArrowForward />
    </NavigateButton>
  );
}
