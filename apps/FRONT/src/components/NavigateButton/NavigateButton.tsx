import { Button } from '@ada/ui';
import type { ReactNode } from 'react';

interface NavigateButtonProps {
  children: ReactNode;
  position: 'prev' | 'next';
  onClick: () => void;
  dataTestId: string;
}

const getStyle = (position: 'prev' | 'next') =>
  position === 'prev' ? { marginRight: 'auto' } : { marginLeft: 'auto' };

export function NavigateButton({
  children,
  position,
  onClick,
  dataTestId,
}: NavigateButtonProps) {
  const style = getStyle(position);

  return (
    <Button
      data-testid={dataTestId}
      onClick={onClick}
      style={{ ...style, backgroundColor: '#aaa' }}
      type='button'
    >
      {children}
    </Button>
  );
}
