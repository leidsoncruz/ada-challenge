import { List } from '@ada/types';
import { Board } from '@ada/ui';
import { Card, CardForm } from '@/components';
import { useCards } from '@/hooks';

function App(): JSX.Element {
  const { data: cards } = useCards();

  return (
    <Board>
      <Board.Column dataTestId='new' title='Novo'>
        <CardForm list={List.TODO} />
      </Board.Column>
      <Board.Column dataTestId='todo' title={List.TODO}>
        {cards[List.TODO].map(({ content, id, title, list }) => (
          <Card content={content} id={id} key={id} list={list} title={title} />
        ))}
      </Board.Column>
      <Board.Column dataTestId='doing' title={List.DOING}>
        {cards[List.DOING].map(({ content, id, title, list }) => (
          <Card content={content} id={id} key={id} list={list} title={title} />
        ))}
      </Board.Column>
      <Board.Column dataTestId='done' title={List.DONE}>
        {cards[List.DONE].map(({ content, id, title, list }) => (
          <Card content={content} id={id} key={id} list={list} title={title} />
        ))}
      </Board.Column>
    </Board>
  );
}

export default App;
