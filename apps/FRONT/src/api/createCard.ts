import type { Card, CardDTO } from '@ada/types';

export const createCard = async (
  token: string,
  body: CardDTO
): Promise<Card> => {
  try {
    const response = await fetch('http://localhost:5001/cards', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(body),
    });

    if (!response.ok) {
      throw new Error(`Create the ${body.title} card failed`);
    }

    return (await response.json()) as Card;
  } catch (error) {
    throw new Error(`An error occurred during creating ${body.title} card`);
  }
};
