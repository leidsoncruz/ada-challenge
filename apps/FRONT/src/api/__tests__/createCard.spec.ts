import type { CardDTO } from '@ada/types';
import { createCard } from '../createCard';
import { CARD } from '../../fixtures';

describe('createCard api', () => {
  beforeEach(() => {
    jest.restoreAllMocks();
  });

  it('calls the right route', async () => {
    jest.spyOn(global, 'fetch').mockResolvedValue({
      status: 201,
      ok: true,
      json: async () => CARD,
    } as Response);

    const body: CardDTO = { title: 'title', content: 'co', list: 'todo' };

    await createCard('token', body);
    expect(global.fetch).toHaveBeenLastCalledWith(
      'http://localhost:5001/cards',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer token',
        },
        body: JSON.stringify(body),
      }
    );
  });
  describe('when error is returned', () => {
    it('should execute request and return error', async () => {
      jest.spyOn(global, 'fetch').mockResolvedValue({
        status: 500,
        ok: false,
      } as Response);

      const body: CardDTO = { title: 'title', content: 'co', list: 'todo' };

      await createCard('token', body).catch((err) => {
        expect(err.message).toEqual(
          'An error occurred during creating title card'
        );
      });
    });
  });
});
