export const deleteCard = async (
  token: string,
  id: string
): Promise<number> => {
  try {
    const response = await fetch(`http://localhost:5001/cards/${id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    if (!response.ok) {
      throw new Error(`Delete the ${id} card failed`);
    }

    return response.status;
  } catch (error) {
    throw new Error(`An error occurred during deleting ${id} card`);
  }
};
