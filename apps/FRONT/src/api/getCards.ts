import type { Card } from '@ada/types';

export const getCards = async (token: string): Promise<Card[]> => {
  try {
    const response = await fetch('http://localhost:5001/cards', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    if (!response.ok) {
      throw new Error('Get cards failed');
    }

    return (await response.json()) as Card[];
  } catch (error) {
    throw new Error('An error occurred during getting cards');
  }
};
