import type { Credentials } from '@ada/types';

export const login = async ({
  username,
  password,
}: Credentials): Promise<string> => {
  try {
    const response = await fetch('http://localhost:5001/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ username, password }),
    });

    if (!response.ok) {
      throw new Error('Login failed');
    }

    const token = (await response.json()) as string;

    return token;
  } catch (error) {
    throw new Error('An error occurred during login');
  }
};
