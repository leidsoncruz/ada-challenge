import type { Card } from '@ada/types';

export const updateCard = async (token: string, body: Card): Promise<Card> => {
  try {
    const response = await fetch(`http://localhost:5001/cards/${body.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(body),
    });

    if (!response.ok) {
      throw new Error(`Update the ${body.title} card failed`);
    }

    return (await response.json()) as Card;
  } catch (error) {
    throw new Error(`An error occurred during uptading ${body.title} card`);
  }
};
