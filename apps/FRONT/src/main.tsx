import React from 'react';
import { createRoot } from 'react-dom/client';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ThemeProvider } from '@ada/ui';
import App from './app';
import { AuthProvider } from '@/contexts';

const el = document.getElementById('root');
if (el) {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        enabled: true,
        keepPreviousData: true,
        retry: 1,
        refetchOnWindowFocus: false,
      },
    },
  });
  const root = createRoot(el);
  root.render(
    <React.StrictMode>
      <QueryClientProvider client={queryClient}>
        <AuthProvider>
          <ThemeProvider>
            <App />
          </ThemeProvider>
        </AuthProvider>
      </QueryClientProvider>
    </React.StrictMode>
  );
} else {
  throw new Error('Could not find root element');
}
