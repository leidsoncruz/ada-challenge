import type { ReactNode } from 'react';
import { createContext, useContext, useEffect } from 'react';
import type { Credentials } from '@ada/types';
import { useLogin } from '@/hooks';

interface AuthContextProps {
  token: string | null;
  isLogged: boolean;
}

export const AuthProviderContext = createContext<AuthContextProps>({
  token: null,
  isLogged: false,
});

const DEFAULT_CREDENTIALS: Credentials = {
  username: 'letscode',
  password: 'lets@123',
};

export function AuthProvider({ children }: { children: ReactNode }) {
  const { mutate: loginMutation, data: token = null } = useLogin();

  useEffect(() => {
    if (!token) loginMutation(DEFAULT_CREDENTIALS);
  }, [loginMutation, token]);

  return (
    <AuthProviderContext.Provider
      value={{
        token,
        isLogged: Boolean(token),
      }}
    >
      {children}
    </AuthProviderContext.Provider>
  );
}

export const useAuthentication = () => {
  const context = useContext(AuthProviderContext);

  return context;
};
