import type { Card, CardDTO } from '@ada/types';
import { useMutation, useQueryClient } from 'react-query';
import { createCard } from '@/api';
import { useAuthentication } from '@/contexts';

export const useCreateCard = () => {
  const { token } = useAuthentication();
  const queryClient = useQueryClient();

  const { mutate, data } = useMutation<Card, Error, CardDTO>(
    (body) => createCard(token ?? '', body),
    {
      onSuccess: (card) => {
        queryClient.setQueryData(['cards'], (prev: Card[] | undefined) => {
          return prev ? [...prev, card] : [card];
        });
      },
    }
  );

  return {
    mutate,
    data,
  };
};
