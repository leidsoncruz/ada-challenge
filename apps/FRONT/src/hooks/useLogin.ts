import { useMutation } from 'react-query';
import type { Credentials } from '@ada/types';
import { login } from '@/api';

export const useLogin = () => {
  const { mutate, data } = useMutation<string, Error, Credentials>(login);

  return {
    mutate,
    data,
  };
};
