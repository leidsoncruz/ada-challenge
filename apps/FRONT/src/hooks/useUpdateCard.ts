import type { Card } from '@ada/types';
import { useMutation, useQueryClient } from 'react-query';
import { updateCard } from '@/api';
import { useAuthentication } from '@/contexts';

export const useUpdateCard = () => {
  const { token } = useAuthentication();
  const queryClient = useQueryClient();

  const { mutate, data } = useMutation<Card, Error, Card>(
    (body) => updateCard(token ?? '', body),
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries('cards');
      },
    }
  );

  return {
    mutate,
    data,
  };
};
