import { useMutation, useQueryClient } from 'react-query';
import { deleteCard } from '@/api';
import { useAuthentication } from '@/contexts';

export const useDeleteCard = () => {
  const { token } = useAuthentication();
  const queryClient = useQueryClient();

  const { mutate, data } = useMutation<number, Error, string>(
    (id) => deleteCard(token ?? '', id),
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries('cards');
      },
    }
  );

  return {
    mutate,
    data,
  };
};
