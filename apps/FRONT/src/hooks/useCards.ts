import { List, type Card } from '@ada/types';
import type { UseQueryResult } from 'react-query';
import { useQuery } from 'react-query';
import { useMemo } from 'react';
import { getCards } from '@/api';
import { useAuthentication } from '@/contexts';

type CardList = Record<List, Card[]>;

const INITIAL_VALUE: CardList = {
  [List.DONE]: [],
  [List.DOING]: [],
  [List.TODO]: [],
};

export const useCards = () => {
  const { token } = useAuthentication();

  const { data, isLoading, error }: UseQueryResult<Card[]> = useQuery({
    queryKey: 'cards',
    queryFn: () => getCards(token ?? ''),
    enabled: Boolean(token),
  });

  const cardList =
    useMemo(() => {
      return data?.reduce(
        (prev, curr) => ({
          ...prev,
          [curr.list]: [...prev[curr.list], curr],
        }),
        INITIAL_VALUE
      );
    }, [data]) || INITIAL_VALUE;

  return {
    data: cardList,
    isLoading,
    error,
  };
};
