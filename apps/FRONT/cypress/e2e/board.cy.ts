Cypress.Commands.add(
  'createCard',
  ({ title, content }: { title: string; content: string }) => {
    cy.get('[data-testid=card-form-create] input[name="title"]').type(title);
    cy.get('[data-testid=card-form-create] textarea[name="content"]').type(
      content
    );

    cy.get('[data-testid=card-form-create] button[type="submit"]').click();
  }
);

Cypress.Commands.add('moveCardForward', (number: number) => {
  cy.get(
    `[data-testid*=card-visual]:contains("New title ${number}") [data-testid=move-forward]`
  ).click();
});

describe('Board page', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3001');
  });

  it('should render an empty board', () => {
    cy.get('[data-testid*=card-visual]').should('not.exist');
  });

  describe('create a card', () => {
    it('should create a card and move it to the TODO column', () => {
      cy.get('[data-testid=card-form-create] input[name="title"]').type(
        'New title'
      );
      cy.get('[data-testid=card-form-create] textarea[name="content"]').type(
        'Content'
      );
      cy.get('[data-testid=card-form-create] button[type="submit"]').click();
      cy.get('[data-testid=card-form-create] input[name="title"]').should(
        'have.value',
        ''
      );
      cy.get('[data-testid=card-form-create] textarea[name="content"]').should(
        'have.value',
        ''
      );
      cy.get(
        '[data-testid=board-column-todo] [data-testid*=card-visual] > [data-testid=card-title]'
      ).contains('New title');
    });
  });

  describe('move a card', () => {
    let randomNumber: number;

    beforeEach(() => {
      randomNumber = Math.floor(Math.random() * 9999);
      cy.createCard({
        title: `New title ${randomNumber}`,
        content: 'new content',
      });
    });

    it('move a card from TODO column to DOING column', () => {
      cy.get(
        `[data-testid=board-column-todo] [data-testid*=card-visual]:contains("New title ${randomNumber}") [data-testid=move-forward]`
      ).click();

      cy.get(
        `[data-testid=board-column-todo] [data-testid*=card-visual]:contains("New title ${randomNumber}")`
      ).should('not.exist');

      cy.get(
        `[data-testid=board-column-doing] [data-testid*=card-visual]:contains("New title ${randomNumber}")`
      ).should('exist');
    });

    it('move a card from DOING column to DONE column', () => {
      cy.moveCardForward(randomNumber);

      cy.get(
        `[data-testid=board-column-doing] [data-testid*=card-visual]:contains("New title ${randomNumber}") [data-testid=move-forward]`
      ).click();

      cy.get(
        `[data-testid=board-column-doing] [data-testid*=card-visual]:contains("New title ${randomNumber}")`
      ).should('not.exist');

      cy.get(
        `[data-testid=board-column-done] [data-testid*=card-visual]:contains("New title ${randomNumber}")`
      ).should('exist');
    });

    describe('when the card is on DONE column', () => {
      it('should not render the forward button and render back button', () => {
        cy.moveCardForward(randomNumber);
        cy.moveCardForward(randomNumber);

        cy.get(
          `[data-testid=board-column-done] [data-testid*=card-visual]:contains("New title ${randomNumber}") [data-testid=move-forward]`
        ).should('not.exist');

        cy.get(
          `[data-testid=board-column-done] [data-testid*=card-visual]:contains("New title ${randomNumber}") [data-testid=move-back]`
        ).should('exist');
      });
    });

    it('move a card from DONE to DOING column', () => {
      cy.moveCardForward(randomNumber);
      cy.moveCardForward(randomNumber);

      cy.get(
        `[data-testid=board-column-done] [data-testid*=card-visual]:contains("New title ${randomNumber}") [data-testid=move-back]`
      ).click();

      cy.get(
        `[data-testid=board-column-done] [data-testid*=card-visual]:contains("New title ${randomNumber}")`
      ).should('not.exist');

      cy.get(
        `[data-testid=board-column-doing] [data-testid*=card-visual]:contains("New title ${randomNumber}")`
      ).should('exist');
    });

    it('move a card from DOING to TODO column', () => {
      cy.moveCardForward(randomNumber);
      cy.get(
        `[data-testid=board-column-doing] [data-testid*=card-visual]:contains("New title ${randomNumber}") [data-testid=move-back]`
      ).click();

      cy.get(
        `[data-testid=board-column-doing] [data-testid*=card-visual]:contains("New title ${randomNumber}")`
      ).should('not.exist');

      cy.get(
        `[data-testid=board-column-todo] [data-testid*=card-visual]:contains("New title ${randomNumber}")`
      ).should('exist');
    });

    describe('when the card is on TODO column', () => {
      it('should not render the back button', () => {
        cy.get(
          `[data-testid=board-column-todo] [data-testid*=card-visual]:contains("New title ${randomNumber}") [data-testid=move-back]`
        ).should('not.exist');

        cy.get(
          `[data-testid=board-column-todo] [data-testid*=card-visual]:contains("New title ${randomNumber}") [data-testid=move-forward]`
        ).should('exist');
      });
    });
  });

  describe('delete a card', () => {
    it('the card should disappears from the board', () => {
      const randomNumber = Math.floor(Math.random() * 9999);
      cy.createCard({
        title: `Card to be deleted ${randomNumber}`,
        content: 'new content',
      });

      cy.get(
        `[data-testid=board-column-todo] [data-testid*=card-visual]:contains("Card to be deleted ${randomNumber}")`
      ).should('exist');

      cy.get(
        `[data-testid=board-column-todo] [data-testid*=card-visual]:contains("Card to be deleted ${randomNumber}") [data-testid=delete-button]`
      ).click();

      cy.get(
        `[data-testid=board-column-todo] [data-testid*=card-visual]:contains("Card to be deleted ${randomNumber}")`
      ).should('not.exist');
    });
  });

  describe('edit card', () => {
    it('update the card values', () => {
      const randomNumber = Math.floor(Math.random() * 9999);
      cy.createCard({
        title: `Card to be edited ${randomNumber}`,
        content: 'new content',
      });

      cy.get(
        `[data-testid=board-column-todo] [data-testid*=card-visual]:contains("Card to be edited ${randomNumber}") [data-testid=edit-button]`
      ).click();

      cy.get(
        `[data-testid=board-column-todo] [data-testid=card-form-edit] input[name="title"][value="Card to be edited ${randomNumber}"]`
      )
        .clear()
        .type(`Updated card ${randomNumber}`);

      cy.get(
        '[data-testid=board-column-todo] [data-testid=card-form-edit] textarea[name="content"]'
      )
        .clear()
        .type(`Content updated by ${randomNumber}`);

      cy.get(
        '[data-testid=board-column-todo] [data-testid=card-form-edit] button[type=submit]'
      ).click();

      cy.get(
        `[data-testid=board-column-todo] [data-testid*=card-visual]:contains("Updated card ${randomNumber}")`
      ).should('exist');
    });

    describe('discard the card changes', () => {
      it('should preserve the previous value', () => {
        const randomNumber = Math.floor(Math.random() * 9999);
        cy.createCard({
          title: `Card to be edited ${randomNumber}`,
          content: 'new content',
        });

        cy.get(
          `[data-testid=board-column-todo] [data-testid*=card-visual]:contains("Card to be edited ${randomNumber}") [data-testid=edit-button]`
        ).click();

        cy.get(
          `[data-testid=board-column-todo] [data-testid=card-form-edit] input[name="title"][value="Card to be edited ${randomNumber}"]`
        )
          .clear()
          .type(`Updated card ${randomNumber}`);

        cy.get(
          '[data-testid=board-column-todo] [data-testid=card-form-edit] textarea[name="content"]'
        )
          .clear()
          .type(`Content updated by ${randomNumber}`);

        cy.get(
          '[data-testid=board-column-todo] [data-testid=card-form-edit] button[type=reset]'
        ).click();

        cy.get(
          `[data-testid=board-column-todo] [data-testid*=card-visual]:contains("Card to be edited ${randomNumber}")`
        ).should('exist');
      });
    });
  });
});
