import type { Card, CardDTO, Credentials } from '@ada/types';
import cors from 'cors';
import { config } from 'dotenv';
import express, { json, type Express } from 'express';
import { sign } from 'jsonwebtoken';
import { v4 as uuidv4 } from 'uuid';
import { log } from '@ada/logger';
import type { CustomMutationRequest } from '@/interfaces';
import { tokenValidation, validateAndLogAlterationOrDeletion } from '@/utils';

config();

export const createServer = (): Express => {
  //@doc: como isto é uma constante vindo de variáveis de ambiente não precisa estar sendo destructor em toda chamada do login
  const { DEFAULT_LOGIN, DEFAULT_PASSWORD, JWT_SECRET = '' } = process.env;

  const app = express();
  let cards: Card[] = [];

  app
    .use(cors())
    .use(json())
    .use((req, _res, next) => {
      log('Request ->', req.path);
      next();
    })
    .use(tokenValidation)
    .use('/cards/:id', (req, res, next) =>
      validateAndLogAlterationOrDeletion(req, res, next, cards)
    );

  app.post('/login', (req: CustomMutationRequest<Credentials>, res) => {
    const { username, password } = req.body;

    if (username === DEFAULT_LOGIN && password === DEFAULT_PASSWORD) {
      const token = sign({ user: DEFAULT_LOGIN }, JWT_SECRET, {
        expiresIn: '1h',
      });
      return res.json(token);
    }
    res.status(401);
    res.end();
  });

  app.get('/cards', (_req, res) => {
    res.json(cards);
    res.end();
  });

  app.post('/cards', (req: CustomMutationRequest<CardDTO>, res) => {
    const { title, content, list } = req.body;

    if (title && content && list) {
      const card: Card = { title, content, list, id: uuidv4() };
      cards.push(card);
      res.status(201).json(card);
    } else return res.sendStatus(400);
  });

  app.put('/cards/:id', (req: CustomMutationRequest<CardDTO>, res) => {
    const { title, content, list } = req.body;
    const id = req.params.id;
    const card = cards.find((x) => x.id === id);

    if (card) {
      card.title = title;
      card.content = content;
      card.list = list;
      return res.status(200).json(card);
    }

    return res.sendStatus(404);
  });

  app.delete('/cards/:id', (req, res) => {
    const { id } = req.params;
    cards = cards.filter((x) => x.id !== id);
    res.json(cards);
  });

  return app;
};
