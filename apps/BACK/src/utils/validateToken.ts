/* eslint-disable no-console -- Logging some info to give a dev feedback */

import { config } from 'dotenv';
import type { NextFunction, Request, Response } from 'express';
import type { JwtPayload } from 'jsonwebtoken';
import { verify } from 'jsonwebtoken';

config();

const { JWT_SECRET = '' } = process.env;

export const tokenValidation = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (req.path !== '/login') {
    try {
      const auth = req.headers.authorization;
      const token = auth?.replace('Bearer ', '');
      if (auth && token) {
        const decoded = verify(token, JWT_SECRET) as JwtPayload & {
          user: string;
        };

        res.locals = { user: decoded.user };
        console.info(
          `JWT Middleware - validated token for user: ${decoded.user}`
        );
      } else throw new Error('token not found');
    } catch (err) {
      const message = err instanceof Error ? err.message : String(err);

      console.info(`JWT Middleware - error validating token\n${message}`);
      res.sendStatus(401);
      return res.end();
    }
  }

  next();
};
