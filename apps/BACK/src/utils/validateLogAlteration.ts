import type { NextFunction, Request, Response } from 'express';
import type { Card, CardDTO } from '@ada/types';
import type { CustomMutationRequest } from '@/interfaces';

export const validateAndLogAlterationOrDeletion = (
  req: CustomMutationRequest<CardDTO>,
  res: Response,
  next: NextFunction,
  cards: Card[]
) => {
  const id = req.params.id;
  const dateTime = new Date().toLocaleString('pt-br');

  if (!id) return res.sendStatus(400);

  const card = cards.find((x) => x.id === id);
  if (!card) return res.sendStatus(404);

  if (req.method === 'PUT') {
    const { title, content, list } = req.body;

    if (!(title && content && list && id)) return res.sendStatus(400);
    console.info(`${dateTime} - Card ${id} - ${card.title} - Alterar`);
  } else if (req.method === 'DELETE') {
    console.info(`${dateTime} - Card ${id} - ${card.title} - Remover`);
  }

  next();
};
