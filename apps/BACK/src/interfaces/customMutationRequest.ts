import type { Request } from 'express';

export interface CustomMutationRequest<T> extends Request {
  body: T;
}
