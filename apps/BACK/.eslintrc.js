/** @type {import("eslint").Linter.Config} */
module.exports = {
  extends: ["@ada/eslint-config/server.js"],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    sourceType: "module",
    project: true
  },
};
