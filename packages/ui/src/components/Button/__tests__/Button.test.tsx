import { render } from '@testing-library/react';
import { Button } from '..';

describe('<Button /> component', () => {
  it('should render a html button with label', () => {
    const { container } = render(<Button>My Label</Button>);

    expect(container).toMatchSnapshot();
  });
});
