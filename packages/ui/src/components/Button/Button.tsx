import type { ButtonHTMLAttributes } from 'react';
import { ButtonStyled } from './Button.styled';

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  variant?: 'solid' | 'outlined';
}

export function Button({ children, variant = 'solid', ...rest }: ButtonProps) {
  return (
    <ButtonStyled variant={variant} {...rest}>
      {children}
    </ButtonStyled>
  );
}
