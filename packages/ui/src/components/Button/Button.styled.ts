import styled from 'styled-components';

export interface ButtonStyledProps {
  variant: 'solid' | 'outlined';
}

export const ButtonStyled = styled.button<ButtonStyledProps>`
  ${({ variant }) => `
    border: ${variant === 'solid' ? 0 : '1px solid'};
    background-color: ${variant === 'solid' ? '#0079d6' : 'transparent'};
  `}
  font-size: 1rem;
  line-height: 1.34;
  font-weight: 700;
  border-radius: 3px;
  white-space: nowrap;
  cursor: pointer;
  max-width: 20vw;
  padding: 0px 15px;
  height: 32px;
  color: #ffffff;
  display: flex;
  align-items: center;
  justify-content: center;
`;
