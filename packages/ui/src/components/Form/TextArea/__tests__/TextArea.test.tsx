import { render } from '@testing-library/react';
import { TextArea } from '..';

describe('<TextArea /> component', () => {
  it('should render the textarea element', () => {
    const { container } = render(<TextArea />);

    expect(container).toMatchSnapshot();
  });

  it('should render a required textarea', () => {
    const { container } = render(<TextArea required />);

    expect(container).toMatchSnapshot();
  });

  it('should render a textarea with placeholder and name', () => {
    const { container } = render(
      <TextArea name='textarea' placeholder='Placeholder...' />
    );

    expect(container).toMatchSnapshot();
  });
});
