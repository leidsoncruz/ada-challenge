import type { InputHTMLAttributes } from 'react';
import { TextAreaStyled } from './TextArea.styeld';

export function TextArea(props: InputHTMLAttributes<HTMLTextAreaElement>) {
  return <TextAreaStyled {...props} />;
}
