import styled from 'styled-components';

export const TextAreaStyled = styled.textarea`
  font-weight: 400;
  font-size: 1rem;
  line-height: 1.34;
  border: 1px solid #cccccc;
  color: #151617;
  padding: 5px;
  width: 100%;
  border-radius: 5px;
`;
