import styled from 'styled-components';

export const InputStyled = styled.input`
  font-weight: 400;
  font-size: 1rem;
  line-height: 1.34;
  border: 1px solid #cccccc;
  color: #151617;
  padding: 15px 5px;
  width: 100%;
  height: 25px;
  border-radius: 5px;
`;
