import { render } from '@testing-library/react';
import { Input } from '..';

describe('<Input /> component', () => {
  it('should render an input element', () => {
    const { container } = render(<Input />);

    expect(container).toMatchSnapshot();
  });

  it('should render a required input', () => {
    const { container } = render(<Input required />);

    expect(container).toMatchSnapshot();
  });

  it('should render input with placeholder and name', () => {
    const { container } = render(
      <Input name='input' placeholder='Placeholder...' />
    );

    expect(container).toMatchSnapshot();
  });
});
