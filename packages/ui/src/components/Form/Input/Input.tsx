import type { InputHTMLAttributes } from 'react';
import { InputStyled } from './Input.styled';

export function Input(props: InputHTMLAttributes<HTMLInputElement>) {
  return <InputStyled {...props} />;
}
