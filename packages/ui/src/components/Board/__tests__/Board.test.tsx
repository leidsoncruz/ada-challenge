import { render } from '@testing-library/react';
import { Board } from '..';

describe('<Board /> component', () => {
  it('should render the board structure', () => {
    const { container } = render(
      <Board>
        <Board.Column title='column 01'>Column 01</Board.Column>
        <Board.Column title='column 02'>Column 02</Board.Column>
        <Board.Column title='column 03'>Column 03</Board.Column>
      </Board>
    );

    expect(container).toMatchSnapshot();
  });
});
