import { BoardStyled } from './Board.styled';
import { Column } from './Column';
import type { BaseProps } from '@/types';

function Board({ children }: BaseProps) {
  return <BoardStyled>{children}</BoardStyled>;
}

Board.Column = Column;

export default Board;
