import {
  ColumnContentStyled,
  ColumnStyled,
  ColumnTitleStyled,
} from './Column.styled';
import type { BaseProps } from '@/types';

interface ColumnProps extends BaseProps {
  title: string;
}

export function Column({
  children,
  title,
  dataTestId = 'default',
}: ColumnProps) {
  return (
    <ColumnStyled data-testid={`board-column-${dataTestId}`}>
      <ColumnTitleStyled>{title}</ColumnTitleStyled>
      <ColumnContentStyled>{children}</ColumnContentStyled>
    </ColumnStyled>
  );
}
