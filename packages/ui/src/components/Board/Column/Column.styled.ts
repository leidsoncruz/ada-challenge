import styled from 'styled-components';

export const ColumnStyled = styled.div`
  width: 100%;
  min-width: 200px;
  display: flex;
  flex-direction: column;
  border-radius: 6px;
  background-color: rgb(246, 248, 250);
  transition: transform 75ms ease 0s;
  border: 1px solid rgb(216, 222, 228);
`;

export const ColumnTitleStyled = styled.h2`
  margin: 0 15px;
`;

export const ColumnContentStyled = styled.div`
  flex-direction: column;
  background-color: rgb(246, 248, 250);
  flex-grow: 1;
  overflow-y: auto;
  gap: 10px;
  display: flex;
  padding: 10px;
`;
