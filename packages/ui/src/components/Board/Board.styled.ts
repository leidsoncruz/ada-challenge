import styled from 'styled-components';

export const BoardStyled = styled.div`
  display: flex;
  min-height: calc(100vh - 20px);
  gap: 1rem;
  width: 100%;
`;
