import { render } from '@testing-library/react';
import { Card } from '..';

describe('<Card /> component', () => {
  it('should render the card structure', () => {
    const { container } = render(
      <Card>
        <Card.Title>Card title</Card.Title>
        <div>Content</div>
        <Card.Footer>Footer</Card.Footer>
      </Card>
    );

    expect(container).toMatchSnapshot();
  });
});
