import styled from 'styled-components';

export const CardStyled = styled.div`
  border-radius: 6px;
  border: 1px solid #d8dee4;
  box-shadow: rgba(140, 149, 159, 0.15) 0px 3px 6px;
  flex-direction: column;
  flex-shrink: 0;
  padding: 8px;
  display: flex;
  gap: 0.4rem;
  background-color: #ffffff;

  & form {
    gap: 0.4rem;
    flex-direction: column;
    display: flex;
  }
`;

export const CardTitleStyled = styled.div`
  font-weight: 700;
  border-bottom: 1px solid #ccc;
  display: flex;
  justify-content: space-between;
`;

export const CardFooterStyled = styled.div`
  height: 40px;
  display: flex;
  justify-content: space-between;
`;
