import type { DetailedHTMLProps, HTMLAttributes } from 'react';
import { CardStyled, CardTitleStyled, CardFooterStyled } from './Card.styled';
import type { BaseProps } from '@/types';

function Card({ children, dataTestId }: BaseProps) {
  return <CardStyled data-testid={dataTestId}>{children}</CardStyled>;
}

Card.Title = function ({ children, dataTestId = 'card-title' }: BaseProps) {
  return <CardTitleStyled data-testid={dataTestId}>{children}</CardTitleStyled>;
};

Card.Footer = function ({ children }: BaseProps) {
  return <CardFooterStyled>{children}</CardFooterStyled>;
};

export default Card;
