import type { DefaultTheme } from 'styled-components';
import {
  createGlobalStyle,
  ThemeProvider as StyledProvider,
} from 'styled-components';
import type { BaseProps } from '@/types';

const GlobalStyle = createGlobalStyle`
  body,
  html {
    margin: 0;
    padding: .3rem;
    font-family: ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont,
    Segoe UI, Roboto, Helvetica Neue, Arial, sans-serif;
    -webkit-text-size-adjust: 100%;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    -webkit-tap-highlight-color: transparent;
    line-height: 1.5;
    tab-size: 4;
  }

  * {
    box-sizing: border-box;
  }
`;

interface ThemeProviderProps extends BaseProps {
  theme?: DefaultTheme;
}

export function ThemeProvider({ children, theme = {} }: ThemeProviderProps) {
  return (
    <StyledProvider theme={theme}>
      <GlobalStyle />
      {children}
    </StyledProvider>
  );
}
