import type { ReactNode } from 'react';

export interface BaseProps {
  children: ReactNode;
  dataTestId?: string;
}
