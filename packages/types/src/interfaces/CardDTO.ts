import { List } from '@/enums';

export interface CardDTO {
  title: string;
  content: string;
  list: List;
}
