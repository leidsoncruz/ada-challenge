export enum List {
  TODO = 'To do',
  DOING = 'Doing',
  DONE = 'Done',
}

export interface Card {
  id: string;
  title: string;
  content: string;
  list: List;
}
