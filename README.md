# Desafio Técnico - Frontend

## O que está dentro

Este monorepo possui as seguintes aplicações/pacotes:

* `@ada/frontend`: Uma aplicação [ReactJS](https://react.dev/) + [ViteJS](https://vitejs.dev/).
* `@ada/backend`: Uma aplicação [Express](https://expressjs.com/).
* `@ada/ui`: Pacote contendo elementos de UI utilizando [Styled Components](https://styled-components.com/).
* `@ada/types`: Um pacote de tipos para ser compartilhado por todo monorepo.
* `@ada/eslint-config`: Configurações do `eslint` para ser usado em todas aplicações e pacotes do monorepo.
* `@ada/typescript-config`: Configurações de `typescript` para ser usado em todas aplicações e pacotes do monorepo.

Todas as aplicações deste monorepo utiliza 100% de [Typescript](https://www.typescriptlang.org/).


## Como correr o projeto

### Local

Este projeto precisa versão do node >= 18. 

É correr o comando para instalar as dependências do projeto no root do projeto com seu gerenciador de pacote. Por exemplo:
 
 - Com yarn: `yarn`.
 - Com npm: `npm install`.

Depois correr o comando `dev`. Com este comando o monorepo corre o server e client. Por exemplo:

- Com yarn: `yarn dev`.
- Com npm `npm run dev`.


### Docker

TBD

Docker está funcional para a aplicação backend. A aplicação de frontend está com um pequeno problema pois mesmo sendo exposto a aplicação, ela não está correndo.

Para correr o docker pode ser executado através de: 

```bash
docker-compose -f docker-compose.yml up --build
```

### Testes de integração

É necessário correr o projeto localmente e depois `yarn workspace @ada/frontend cypress:open`


### Testes unitários

Os testes podem ser executados através do comando:

```bash
yarn workspace @ada/frontend test:unit --watch
```

Não tive tempo para criar todos os outros cenários de testes unitários, porém os mesmos seguem o padrão dos testes criados. Para o serviço de api do frontend: testar se é o endpoint está sendo chamado corretamente com os devidos parâmetros. Ao ocorrer um erro, retornar o erro corretamente.

## Explicações

### Monorepo

A escolha de utilizar monorepo foi por facilitar o desenvolvimento local das aplicações e pacotes. Alguns deles como o frontend e backend possuem o mesmo tipo de interfaces e uma codebase semelhante. Com o monorepo conseguimos compartilhar esses recursos facilmente.

### Biblioteca UI

Para separar responsabilidades de UI e aplicação, foi criado um pacote `@ada/ui`. Foi criado os componentes da raíz pelos seguintes motivos:

- O uso de frameworks como tailwind ou outros não demonstra nenhuma skill de css, apenas o conhecimento do uso da framework.
- Um dos motivos do uso de `styled-components` é sobretudo para reutilizar componentes com uma escrita limpa de CSS. Sua sintaxe é bem identica ao SCSS que é o que mais uso.
- Board usando Flex ao invés de Grid:
  - Ambos são sistemas de layout, porém a principal diferença é que Flex é desenhado para layouts de uma dimensão (coluna **ou** linha). Já a Grid é desenhada para layouts de duas dimensões (coluna **e** linha).

**Melhorias**:

- Adicionar Storybook para testar e validar os componentes.
- Criar palette, cores e etc para não precisar passar styles direto para os componentes (Ex: delete button). Com isso criamos uma consistência. 

### Backend

- Converti a aplicação para utilizar 100% Typescript.
- Movi a desestruturação da process.env para fora do endpoint de login. Visto que é uma constante, não é preciso desestruturar toda vez que o endpoint de login é chamado.
- Faria uma melhoria para o servidor aceitar PATCH também. Assim a atualização ao mover o card de uma coluna para outra não precisa passar o objeto do card inteiro novamente. Com isto, reduziríamos o tráfego de data.

### Frontend

### Aplicação

A aplicação está completamente navegável por keyboard e parcialmente resposiva.

**Melhorias**: 

- Melhorar a responsividade.
- Melhorar visualmente card e board.
- Atualmente é preciso passar o token em todo hook.
  - Pode ser criado um wrapper para abstrair isto.
  - Pode ser criado um middleware/interceptor para injetar o token.
- Adicionar toast para dar visibilidade quando alguma ação foi feita com sucesso ou falha. O [react-hot-toast](https://react-hot-toast.com/) é uma ótima library para isto
- Criar uma pipeline para correr testes unitários e integração antes de o código ser merged no branch.

### Tests

Foi adicionado testes unitários e e2e para o frontend. Onde o teste unitário para a página e os componentes compostos não é importante para o frontend. O importante são as funcionalidades e que as mesmas são testadas usando o e2e com cypress.

A estrutura dos testes e2e foi organizado por página. Com esta abordagem conseguimos manter uma estrutura mais clara que cada arquivo corresponde à uma página específica. Cada arquivo de teste é focado em testar o comportamento de uma página específica provendo uma separação clara das suas preocupações.

Os testes unitários tem como responsabilidade garantir que as funções de API estão chamando o endpoint corretamente e passando os dados corretamente. Também tem a responsabilidade de testar os hooks.

Os testes devem ser independentes de forma como que o resultado do primeiro teste não afete. Por isto que nos testes de e2e para todo teste de mudança de cartão, um novo é criado e movido até onde desejamos para realizar o teste.

Foi adicionado testes para parte visualmente no pacote de UI. Usamos o snapshot pois não importa apenas que o componente está sendo renderizado, queremos garantir que os estilos passados estão sendo passados. Adicionei o `jest-styled-component` para que nos snapshots os valores do CSS sejam postos inline e com isso, caso algum valor mude, o snapshot falha e sabemos exatamente aonde.


**Melhorias**: 

- Adicionar teste de contrato para evitar que a aplicação quebre caso a API mude seu contrato.
- Adicionar os tests unitários restantes para hooks e api.
- Fazer split dos testes e2e por ações. Assim cada pasta corresponde à uma página mas dentro dela tem as ações separadas por arquivos.

### Busca de dados

Eu utilizei uma biblioteca para data fetching pois a mesma tem vantagens como fazer caching dos requests, invalidar o cache (muito usado após a alteração de um dado). Com isto conseguimos uma velocidade para implementação de uma solução robusta que poderia inclusive ser usada em produção.

### ViteJS

Como a aplicação só tem uma rota usar NextJS seria overengineering para isso, pois features como routing, chamadas de api internas e etc não são necessárias. Utilizei ViteJS ao invés de React create app pois o hot module repacement (HMLR) do ViteJS é mais rápido por causa que tem poucos modules nativos importados. Fazendo com que o desenvolvimento local seja mais rápido.

Para um code final o Vite tbm produz um build otimizado que faz com que seus bundles sejam menores e mais eficientes e com isto temos um load da aplicação mais rápido.


### Git

Uso conventional commits para criar meus commits. 